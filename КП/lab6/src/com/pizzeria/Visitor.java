package com.pizzeria;

import java.util.ArrayList;

public class Visitor {
    private int number;
    private String surname;
    private String deliveryAddress;
    private int desiredDeliveryTime;
    private ArrayList<String> orderList = new ArrayList<>();

    public int getNumber() {
        return number;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public int getDesiredDeliveryTime() {
        return desiredDeliveryTime;
    }

    public String getSurname() { return surname; }
    public ArrayList<String> getOrderList() {
        return orderList;
    }

    public Visitor(int number, String surname, String deliveryAddress, int desiredDeliveryTime, ArrayList<String> orderList) {
        this.number = number;
        this.surname = surname;
        this.deliveryAddress = deliveryAddress;
        this.desiredDeliveryTime = desiredDeliveryTime;
        this.orderList = orderList;
    }
    @Override
    public String toString()
    {
        return "Order number: " + number + "; " + "Surname: " + surname + "; " + "Address: " + deliveryAddress + "; " + "Desire delivery time" + desiredDeliveryTime + "; " + "Order list: " + orderList;
    }
    public int getOrderListSize()
    {
        return orderList.size();
    }

}
