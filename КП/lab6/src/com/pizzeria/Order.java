package com.pizzeria;

import java.util.ArrayList;

public class Order {
    private int number;
    private String visitorSurname;
    private int deliveryTime;
    private ArrayList<String> pizzaList;


    public ArrayList<String> getPizzaList() {
        return pizzaList;
    }

    public void setPizzaList(ArrayList<String> pizzaList) {
        this.pizzaList = pizzaList;
    }

    public String getVisitorSurname() {
        return visitorSurname;
    }

    public void setVisitor(String visitorSurname) {
        this.visitorSurname = visitorSurname;
    }

    public int getDeliveryTime() {
        return deliveryTime;
    }
    public int getNumber() {
        return number;
    }

    public void setDeliveryTime(int deliveryTime) {
        this.deliveryTime = deliveryTime;
    }
    public Order(int number, String visitorSurname, int deliveryTime, ArrayList<String> pizzaList) {
        this.pizzaList = pizzaList;
        this.visitorSurname = visitorSurname;
        this.deliveryTime = deliveryTime;
        this.number = number;
    }
    @Override
    public String toString()
    {
      return "Number of order: " + number + "; " + "Visitor surname:" + visitorSurname + "; " + "Delivery time: " + deliveryTime + " minutes" + "; " + "order list: " + pizzaList;
    }

}
