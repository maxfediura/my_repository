package com.pizzeria;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        MenuManager menuManager = new MenuManager();
        menuManager.readFiles();
        menuManager.sortOrdersByDeliveryTime();
        menuManager.createListOfAddress();
        menuManager.verifyGPizzaOrder("Peperoni");
        menuManager.createMapWithPizzaListAndVisitors();
        menuManager.CreateTimeList();
    }
}
