package com.pizzeria;

import java.util.HashMap;
import java.util.Map;

public class Pizzeria{
    private Map<Integer,Pizza> pizzaMap = new HashMap<>();
    private Map<Integer,Visitor> visitorMap = new HashMap<>();

    public Map<Integer, Pizza> getPizzaMap() {
        return pizzaMap;
    }

    public void setPizzaMap(Map<Integer, Pizza> pizzaMap) {
        this.pizzaMap = pizzaMap;
    }

    public Map<Integer, Visitor> getVisitorMap() {
        return visitorMap;
    }

    public void setVisitorMap(Map<Integer, Visitor> visitorMap) {
        this.visitorMap = visitorMap;
    }
}