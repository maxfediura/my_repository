package com.pizzeria;

import java.util.ArrayList;

public class Pizza {
    private String name;
    private int weight;
    private int price;
    private ArrayList<String> productList;

    public Pizza(String name, int weight, int price, ArrayList<String> productList) {
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.productList = productList;
    }
    public Pizza(){}
    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getPrice() {
        return price;
    }

    public ArrayList<String> getProductList() {
        return productList;
    }
    @Override
    public String toString()
    {
        return "Pizza name: " + name + "; " + "Weight: " + weight + "gram; " + "Price: " + price + "UAH; " + "Product list: " + productList;
    }

}
