package com.pizzeria;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MenuManager {
    private static String path1 = "D:\\3 курс\\КП\\lab6\\src\\menu.txt";
    private static String path2 = "D:\\3 курс\\КП\\lab6\\src\\Order.txt";
    private static String path3 = "D:\\3 курс\\КП\\lab6\\src\\Visitors.txt";
    private ArrayList<Order> orderList = new ArrayList<>();
    private ArrayList<Pizza> menu = new ArrayList<>();
    private ArrayList<Visitor> visitorList = new ArrayList<>();


    public void readFiles() throws IOException {
        ArrayList<String> stringList;
        BufferedReader br1 = Files.newBufferedReader(Paths.get(path1));
        BufferedReader br2 = Files.newBufferedReader(Paths.get(path2));
        BufferedReader br3 = Files.newBufferedReader(Paths.get(path3));
        stringList = (ArrayList<String>) br1.lines().collect(Collectors.toList());
        for (String s : stringList) {
            String[] token;
            String name = null;
            int weight = 0;
            int price = 0;
            ArrayList<String> newArray = new ArrayList<>();
            token = s.split("[,()]");
            for (int i = 0; i < token.length; i++) {
                if (i == 0)
                    name = token[i];
                if (i == 1)
                    weight = Integer.parseInt(token[i]);
                if (i == 2)
                    price = Integer.parseInt(token[i]);
                if (i > 2)
                    newArray.add(token[i]);
            }
            menu.add(new Pizza(name, weight, price, newArray));
        }
        stringList = (ArrayList<String>) br2.lines().collect(Collectors.toList());
        for (String s : stringList) {
            String[] token;
            int number = 0;
            String surname = null;
            int deliveryTime = 0;
            ArrayList<String> newArray = new ArrayList<>();
            token = s.split("[,()]");
            for (int i = 0; i < token.length; i++) {
                if (i == 0)
                    number = Integer.parseInt(token[i]);
                if (i == 1)
                    surname = token[i];
                if (i == 2)
                    deliveryTime = Integer.parseInt(token[i]);
                if (i > 2)
                    newArray.add(token[i]);
            }
            orderList.add(new Order(number, surname, deliveryTime, newArray));
        }

        stringList = (ArrayList<String>) br3.lines().collect(Collectors.toList());
        for (String s : stringList) {
            String[] token;
            int number = 0;
            String surname = null;
            String address = null;
            int desireDeliveryTime = 0;
            ArrayList<String> newArray = new ArrayList<>();
            token = s.split("[,()]");
            for (int i = 0; i < token.length; i++) {
                if (i == 0)
                    number = Integer.parseInt(token[i]);
                if (i == 1)
                    surname = token[i];
                if (i == 2)
                    address = token[i];
                if (i == 3)
                    desireDeliveryTime = Integer.parseInt(token[i]);
                if (i > 3)
                    newArray.add(token[i]);
            }
            visitorList.add(new Visitor(number, surname, address, desireDeliveryTime, newArray));
        }
    }
    public void sortOrdersByDeliveryTime() {
        System.out.println("list of orders sorted by delivery time:");
        orderList.stream().sorted(Comparator.comparingInt(Order::getDeliveryTime)).forEach(order -> System.out.println(order.toString()));
    }
    public void createListOfAddress() {
        System.out.println("List address of visitors who order more then 2 pizza:");
        visitorList.stream().filter(visitor -> visitor.getOrderList().size() > 2).forEach(visitor -> System.out.println(visitor.getDeliveryAddress()));
    }
    public void verifyGPizzaOrder(String s) {
        System.out.print("the largest number of ordered pizzas:");
        System.out.println(visitorList.stream().filter(list -> list.getOrderList().contains(s)).count());
    }
        public void createMapWithPizzaListAndVisitors()
    {
        System.out.println("map with pizza and visitors who ordered this pizza");
        Map<String,ArrayList<String>>map1 = new HashMap<>();
        List<Visitor> newVisitors = new ArrayList<>();
        for(Pizza pizza : menu)
        {
            ArrayList<String> surnameList = new ArrayList<>();
            for(Visitor visitor : visitorList)
            {
                visitor.getOrderList().stream().filter(list -> list.contains(pizza.getName())).forEach(visitor1 ->{surnameList.add(visitor.getSurname());});
            }
            map1.put(pizza.getName(),surnameList);
        }
        for(String key : map1.keySet())
        {
            System.out.println(key + ": " + map1.get(key));
        }
    }
    public void CreateTimeList() {
        System.out.println("List of late orders");
        ArrayList<Order> orders = new ArrayList<>();
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < orderList.size(); i++) {
            final int[] k = {0};
            int finalI = i;
            orderList.stream().filter(order -> orderList.get(finalI).getDeliveryTime()>visitorList.get(finalI).getDesiredDeliveryTime()).forEach(order1 -> {
                k[0] = orderList.get(finalI).getDeliveryTime() - visitorList.get(finalI).getDesiredDeliveryTime();
                integers.add(k[0]);
                orders.add(orderList.get(finalI));
            });
        }
        List<Order> orders1 =  orders.stream().distinct().toList();
        List<Integer> integers1 = integers.stream().distinct().toList();

        for(int i = 0; i <integers1.size();i++)
        {
            System.out.println("Number of Order: " + orders1.get(i).getNumber() + " time overdue: " + integers1.get(i));
        }
    }
}

