package com.fishingShop;

public class FishingShop {
    public static void main(String[] args) {
        FishingManager fishingManager = new FishingManager();
        fishingManager.fillDishes();
        System.out.println("Products:");
        fishingManager.printProducts(fishingManager.products);
        System.out.println();
        System.out.println("Products(sorting by name):");
        fishingManager.printProducts(fishingManager.SortByName(true));
        System.out.println();
        System.out.println("Products(sorting by time):");
        fishingManager.printProducts(fishingManager.sortByTime(true));
        System.out.println();
        System.out.println("Products(sorting by price):");
        fishingManager.printProducts(fishingManager.SortByPrice(true));
        System.out.println();
        System.out.println("Products(sorting by quantity):");
        fishingManager.printProducts(fishingManager.sortByQuantity(true));
        System.out.println();
        System.out.println("Products(finding by type):");
        fishingManager.printProducts(fishingManager.FindProductByType(Product.Type.SummerFishing));
    }
}