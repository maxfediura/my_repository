package com.fishingShop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FishingManager {
    public ArrayList<Product> products = new ArrayList<>();

    public void fillDishes() {
        products.add(new Product("Cheese casserole", 5.05, 150,20, Product.Type.SummerFishing));
        products.add(new Product("Omelet", 3.67, 130,25, Product.Type.SummerFishing));
        products.add(new Product("Dumplings", 4.15, 110,15, Product.Type.SummerFishing));
        products.add(new Product("Cheesecake", 6.00, 80,6, Product.Type.SummerFishing));
        products.add(new Product("Morning set", 5.84, 240,35, Product.Type.ExtremeFishing));
        products.add(new Product("Dining set", 7.29, 360,28, Product.Type.ExtremeFishing));
        products.add(new Product("Evening set", 6.44, 270,50, Product.Type.ExtremeFishing));
        products.add(new Product("Zucchini pancakes", 8.08, 115,13, Product.Type.WinterFishing));
        products.add(new Product("Vegetable stew", 7.53, 122,56, Product.Type.WinterFishing));
        products.add(new Product("Italian patent", 5.99, 101,36, Product.Type.WinterFishing));
        products.add(new Product("snack", 3.00, 90,10, Product.Type.ExtremeFishing));
        products.add(new Product("Vegetable bulgur", 10.00, 140,48, Product.Type.WinterFishing));

    }
    public void printProducts(List<Product> lp)
    {
        for (Product p : lp)
        {
            System.out.println(p.toString());
        }
    }


    public List<Product> FindProductByType(Product.Type type)
    {
        List<Product> lp = new ArrayList<>();
        for (Product p : products)
        {
            if (p.getType() == type)
            {
                lp.add(p);
            }
        }
        return lp;
    }



    public static class NameComparator implements Comparator<Product>
    {
        @Override
        public int compare(Product p1, Product p2)
        {
            return p1.getName().compareTo(p2.getName());
        }
    }

    class PriceComparator implements Comparator<Product>
    {
        @Override
        public int compare(Product p1, Product p2)
        {
            return Double.compare(p1.getPrice(), p2.getPrice());
        }
    }
    //public inner
    public List<Product> SortByPrice(boolean b){
        List<Product> copyProducts = new ArrayList<>(products);
        copyProducts.sort(b ? new PriceComparator() : Collections.reverseOrder(new PriceComparator()));
        return copyProducts;
    }

    public List<Product> SortByName(boolean b){
        List<Product> copyProducts = new ArrayList<>(products);
        copyProducts.sort(b ? new NameComparator() : Collections.reverseOrder(new NameComparator()));
        return copyProducts;
    }

    // anonymous
    public List<Product> sortByQuantity(boolean b) {
        List<Product> copyProducts = new ArrayList<>(products);
        copyProducts.sort(new Comparator<>() {
            @Override
            public int compare(Product p1, Product p2) {
                int res = Integer.compare(p1.getQuantity(), p2.getQuantity());
                return b ? res : -res;
            }
        });
        return copyProducts;
    }
    public List<Product> sortByTime(boolean b) {
        List<Product> copyProducts = new ArrayList<>(products);
        copyProducts.sort((p1, p2) -> {
            int res = Integer.compare(p1.getTime(), p2.getTime());
            return b ? res : -res;
        });
        return copyProducts;
    }
}