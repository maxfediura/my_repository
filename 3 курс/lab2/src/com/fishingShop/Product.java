package com.fishingShop;

public class Product {
    private double price;
    private int quantity;
    private String name;
    private int time;
    public enum Type
    {
        WinterFishing,
        SummerFishing,
        ExtremeFishing
    }
    private Type type;
    @Override
    public String toString()
    {
        return  name + " | " + "type:" + type.toString() +  " | " + "quantity:" + quantity + " | " +  "price:" + price + " dollars| " + "delivery time " + time + " days" ;
    }
    public Product(String name, double price, int quantity, int time, Type type)
    {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.time = time;
        this.type = type;
    }
    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getTime() { return time; }

    public String getName() {
        return name;
    }

    public Type getType() { return type; }
}
