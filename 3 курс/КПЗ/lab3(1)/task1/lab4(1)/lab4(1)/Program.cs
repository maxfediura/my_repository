﻿

namespace lab41
{
    interface IFood
    {
        void Eat(IFood food);
    }
    interface ILiquid
    {
        void drink();
    }
    abstract class Dish:IFood
    {
        protected string name;
        public void Eat(IFood food)
        {
            if(food is Dish)
            {
                Console.WriteLine("eat " + ((Dish)food).name);
            }    
        }
    }
    abstract class Drink : ILiquid
    {
        protected string name;
        public void drink(ILiquid liquid)
        {

            if(liquid is Drink)
            {
                Console.WriteLine("drink " + ((Drink)liquid).name);
            }    
        }

        public void drink()
        {
            throw new NotImplementedException();
        }
    }

}
