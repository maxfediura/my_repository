﻿namespace ConsoleApp1
{
   class Program
    {
        static void Main(string[] args)
        {
            int[] threadCount = { 1, 2, 4, 8, 10 };
            Manager manager = new Manager();
            for(int i = 0; i < threadCount.Length; i++)
            {
                manager.ThreadCount = threadCount[i];
                manager.methodTask();
                manager.methodPLINQ();
                manager.methodParallel();
                manager.methodThreadPool();
            }
            manager.WriteTimeTable();
        }
    }
}