﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Manager
    {
        private int arraySize = 1000000;
        private Stopwatch stopwatch;
        private List<int[]> list = new List<int[]>();
        public int ThreadCount { get; set; } = 1;
        private List<string> timeTask = new List<string>();
        private List<string> timePLINQ = new List<string>();
        private List<string> timeParallel = new List<string>();
        private List<string> timeThreadPool = new List<string>();
        public Manager()
        {
            list = new List<int[]>();
            stopwatch = new Stopwatch();
        }

        public List<int[]> createList()
        {
            int elementsInOneThread = (arraySize / ThreadCount);
            List<int[]> list = new List<int[]>(ThreadCount);
            for (int i = 0; i < ThreadCount; i++)
            {
                list.Add(new int[elementsInOneThread]);
            }
            foreach (int[] element in list)
            {
                for (int i = 0; i < elementsInOneThread; i++)
                {
                    element[i] = new Random().Next(1, 50);
                }
            }
            return list;
        }

        public async Task methodTask()
        {
            stopwatch.Start();

            List<Task<int>> tasks = new List<Task<int>>(ThreadCount);
            tasks.AddRange(list.Select(list => Task.Run(list.Sum)));

            double sum = (await Task.WhenAll(tasks)).Sum();

            double arithmeticMean = sum / arraySize;

            stopwatch.Stop();
            TimeSpan elapsed = stopwatch.Elapsed;
            timeTask.Add("Thread count:" + ThreadCount + "\n time:" + elapsed);
        }

        public void methodPLINQ()
        {
            stopwatch.Start();

            double sum = list.AsParallel().WithDegreeOfParallelism(ThreadCount).Select(x => x.Sum()).Sum();

            double numOfElements = list.AsParallel().WithDegreeOfParallelism(ThreadCount).Select(x => x.Count()).Sum();
            double arithmeticMean = sum / numOfElements;

            stopwatch.Stop();
            TimeSpan elapsed = stopwatch.Elapsed;
            timePLINQ.Add("Thread count:" + ThreadCount + "\n time:" + elapsed);
        }
        //IEnumerable
        public void methodParallel()
        {
            stopwatch.Start();

            double sum = 0;
            Parallel.ForEach(list, partOfArray =>
            {
                sum += partOfArray.Sum();
            });

            int numOfElements = 0;
            Parallel.ForEach(list, partsOfArray =>
            {
                int count = 0;
                for (int i = 0; i < partsOfArray.Length; ++i)
                {
                    ++count;    
                }
                numOfElements += count;
            });
            double arithmeticMean = sum / numOfElements;

            stopwatch.Stop();
            TimeSpan elapsed = stopwatch.Elapsed;
            timeParallel.Add("Thread count:" + ThreadCount + "\n time:" + elapsed);
        }

        public void methodThreadPool()
        {
            stopwatch.Start();

            double sum = 0;
            int numOfElements = 0;
            double arithmeticMean = 0;

            foreach (int[] x in list)
            {
                x.ToList();
                using ManualResetEvent resetEvent = new ManualResetEvent(false);

                ThreadPool.QueueUserWorkItem(
                    new WaitCallback(y =>
                    {
                        sum = x.Sum();

                        numOfElements = x.Count();

                        arithmeticMean = sum / numOfElements;

                        resetEvent.Set();
                    }));
                resetEvent.WaitOne();
            }
            stopwatch.Stop();
            TimeSpan elapsed = stopwatch.Elapsed;
            timeThreadPool.Add("Thread count:" + ThreadCount + "\n time:" + elapsed);
        }

        public void WriteTimeTable()
        {
            Console.WriteLine("-------------");
            Console.WriteLine("Task");
            foreach (string s in timeTask)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine("-------------");
            Console.WriteLine("PLINQ");
            foreach (string s in timePLINQ)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine("-------------");
            Console.WriteLine("Parallel");
            foreach (string s in timeParallel)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine("-------------");
            Console.WriteLine("ThreadPool");
            foreach (string s in timeThreadPool)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine("-------------");

        }
    }
}