﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> valuesForThreadNum = new List<int>() { 1, 2, 4, 10 };

            LabManager lab = new LabManager();

            foreach (int num in valuesForThreadNum)
            {
                lab.NumOfThreads = num;

                lab.CalculateUsingPLINQ();
                lab.CalculateUsingParallel();
                lab.CalculateUsingThreadPool();
                lab.CalculateUsingTask();
                Thread.Sleep(5);
            }

            lab.PrintTimeSpans();
            Console.ReadLine();
        }
    }
}
