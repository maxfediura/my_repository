﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        private static int numberOfElements = 5_000;
        private static int minRandom = 0;
        private static int maxRandom = 32_767;

        private static Random rand = new Random();

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("File path: ");
            string path = Console.ReadLine();
            bool songStarted = false;

            while (path != "" && !songStarted)
            {
                if (path == "Bandera")
                {
                    _ = WriteSongAsync();
                    break;
                }
                else if (!File.Exists(path) && path != " ")
                {
                    _ = WriteNumbersIntoFileAsync(path);
                }
                else
                {
                    Console.WriteLine("Wrong file path or file already exists");
                }

                Console.Write("File path: ");
                path = Console.ReadLine();
            }

            Console.ReadLine();
        }

        static async Task WriteNumbersIntoFileAsync(string filePath)
        {
            Console.WriteLine($"Started writing into {filePath}");

            string resultTime = await Task.Run(() => WriteNumbersIntoFile(filePath));

            Console.WriteLine($"{filePath} finished in {resultTime} using thread {Thread.CurrentThread.ManagedThreadId}");
        }

        static string WriteNumbersIntoFile(string filePath)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            using (StreamWriter sw = File.CreateText(filePath))
            {
                for (int i = 0; i < numberOfElements; i++)
                {
                    Thread.Sleep(5);
                    int numberToWrite = rand.Next(minRandom, maxRandom);
                    sw.WriteLineAsync($"{i} - {numberToWrite}");
                }
            }

            stopWatch.Stop();
            return stopWatch.Elapsed.ToString();
        }

        static async Task WriteSongAsync()
        {
            await Task.Run(() =>
            {
                Console.WriteLine("Started writing song");

                for (int i = 0; i < song.Length; ++i)
                {
                    Console.Write(song[i]);
                    Thread.Sleep(100);
                }

                Console.WriteLine("\nFinished writing song");
            });
        }

        #region

        private static string song = "\nБатько наш — Бандера, Україна — мати. Ми за Україну будем воювати!\n" +
        "Батько наш — Бандера, Україна — мати. Ми за Україну будем воювати!\n" +
        "Ой, у лісі, лісі, під дубом зеленим. Там лежить повстанець тяженько ранений.\n" +
        "Ой, у лісі, лісі, під дубом зеленим. Там лежить повстанець тяженько ранений.\n" +
        "Ой, лежить він, лежить, терпить тяжкі муки. Без лівої ноги, без правої руки.\n" +
        "Ой, лежить він, лежить, терпить тяжкі муки. Без лівої ноги, без правої руки.\n" +
        "Як прийшла до нього рідна мати його. Плаче і ридає, жалує його.\n" +
        "Як прийшла до нього рідна мати його. Плаче і ридає, жалує його.\n" +
        "Мами ж наші, мами, не плачте за нами. Не плачте за нами гіркими сльозами.\n" +
        "Мами ж наші, мами, не плачте за нами. Не плачте за нами гіркими сльозами.\n" +
        "Ой, сину ж мій, сину, вже навоювався. Без правої ручки, без ніжки зостався.\n" +
        "Ой, сину ж мій, сину, вже навоювався. Без правої ручки, без ніжки зостався.\n" +
        "Батько наш — Бандера, Україна — мати. Ми за Україну будем воювати!\n" +
        "Батько наш — Бандера, Україна — мати. Ми за Україну будем воювати!\n";
        #endregion
    }
}
