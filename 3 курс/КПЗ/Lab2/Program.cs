﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
            {
                List<Dish> dishes = new List<Dish>();
            dishes.Add(new Dish("Cheese casserole", 5.05D, 150, 20, Dish.Type.ChildrenMenu));
            dishes.Add(new Dish("Omelet", 3.67D, 130, 25, Dish.Type.ChildrenMenu));
            dishes.Add(new Dish("Dumplings", 4.15D, 110, 15, Dish.Type.ChildrenMenu));
            dishes.Add(new Dish("Cheesecake", 6.0D, 80, 6, Dish.Type.ChildrenMenu));
            dishes.Add(new Dish("Morning set", 5.84D, 240, 35, Dish.Type.SetLunches));
            dishes.Add(new Dish("Dining set", 7.29D, 360, 28, Dish.Type.SetLunches));
            dishes.Add(new Dish("Evening set", 6.44D, 270, 50, Dish.Type.SetLunches));
            dishes.Add(new Dish("Zucchini pancakes", 8.08D, 115, 13, Dish.Type.VegetarianMenu));
            dishes.Add(new Dish("Vegetable stew", 7.53D, 122, 56, Dish.Type.VegetarianMenu));
            dishes.Add(new Dish("Italian patent", 5.99D, 101, 36, Dish.Type.VegetarianMenu));
            dishes.Add(new Dish("snack", 3.0D, 90, 10, Dish.Type.SetLunches));
            dishes.Add(new Dish("Vegetable bulgur", 10.0D, 140, 48, Dish.Type.VegetarianMenu));

            Console.WriteLine("list with using - Select:");
            IEnumerable<string> task1 = dishes.Select(d => d.Name);
                foreach(var i in task1)
                {
                    Console.WriteLine(i);
                }

                Console.Write("\n");
            Console.WriteLine("list with using - Where:");
            IEnumerable<Dish> task2 = dishes.Where(d => d.type == Dish.Type.ChildrenMenu);
                foreach (var item in task2)
                {
                    Console.WriteLine(item.Name);
                }

                Console.Write("\n");

            Console.WriteLine("item from Dictionary:");
            Dictionary<int, Dish> keyValuePairs = new Dictionary<int, Dish>();
            keyValuePairs.Add(1,new Dish("Vegetable stew", 7.53D, 122, 56, Dish.Type.VegetarianMenu));
            keyValuePairs.Add(2,new Dish("Italian patent", 5.99D, 101, 36, Dish.Type.VegetarianMenu));
            keyValuePairs.Add(3,new Dish("snack", 3.0D, 90, 10, Dish.Type.SetLunches));
            keyValuePairs.Add(4,new Dish("Vegetable bulgur", 10.0D, 140, 48, Dish.Type.VegetarianMenu));
            Console.WriteLine(keyValuePairs[1].Name);


                Console.Write("\n");
            Console.WriteLine("Number of char (e) in :Cheese casserole");
            Console.WriteLine(dishes[0].Name.CharCount('e'));
                Console.Write("\n");
            Console.WriteLine("Anonim class:");
            var admin = new { Surname = "Fediura", Name = "Max", PhoneNumber = "+380970903214", Email = "fedyura20020603@gmail.com" };
                Console.WriteLine(String.Format("Surname: {0}\nName: {1}\nPhone number: {2}\nEmail: {3}\n", admin.Surname, admin.Name, admin.PhoneNumber, admin.Email));


                Console.Write("\n");
                Console.WriteLine("Sorted list using method DishComparer():");
            dishes.Sort(new DishComparer());
                foreach (var item in dishes)
                {
                    Console.WriteLine(item.Name);
                }

                Console.Write("\n");
                Console.WriteLine("Dish array:");
                Dish[] dishArr = dishes.ToArray();
                foreach (var item in dishArr)
                {
                    Console.WriteLine(item.Name);
                }

                Console.Write("\n");
                Console.WriteLine("Sorted list by using method:OrderBy():");
                dishes = dishes.OrderBy(d => d.Name).ToList();
                foreach (var item in dishes)
                {
                    Console.WriteLine(item.Name);
                }

                Console.ReadLine();
            }

    }

    public static class StringExtension
    {
        public static int CharCount(this string str, char c)
        {
            int counter = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == c)
                    counter++;
            }
            return counter;
        }
    }
}
