﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Dish : IComparable<Dish>
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Weight { get; set; }
        public Type type { get; set; }
        public enum Type
        {
            VegetarianMenu,
            ChildrenMenu,
            SetLunches
        }
        public Dish(String name, double price, int weight, int time, Dish.Type type)
        {
            this.Name = name;
            this.Price = price;
            this.Weight = weight;
            this.type = type;
        }

        public int CompareTo(Dish obj)
        {
            Dish d = obj as Dish;
            return d.Name[0].CompareTo(d.Name[0]);
        }
    }

    class DishComparer : IComparer<Dish>
    {
        public int Compare(Dish d1, Dish d2)
        {
            if (d1.Name[0] > d2.Name[0])
                return 1;
            else if (d1.Name[0] < d2.Name[0])
                return -1;
            else
                return 0;
        }
    }
}
