﻿using System;

namespace lab04
{
    interface IProduct
    {
        void Eat(IProduct product);
    }
    interface IHuman
    {
        void Talk(IHuman human);
    }
    interface IStuff
    {
        void Work(IStuff stuff);
    }

    abstract class Dish : IProduct
    {
        protected string name;

        public void Eat(IProduct product)
        {
            if(product is Dish)
            {
                Console.WriteLine("Eat dish - " + ((Dish)product).name);
            }
        }
    }
    abstract class Drink : IProduct
    {
        protected string name;
        public void Eat(IProduct product)
        {
            if (product is Drink)
            {
                Console.WriteLine("Dring - " + ((Drink)product).name);
            }
        }
    }
    abstract class Personal : IHuman
    {
        protected string name;
        public Personal(string name)
        {
            this.name = name;
        }
        public void Talk(IHuman human)
        {
            if (human is Personal)
            {
                Console.WriteLine(((Personal)human).name + " is working here");
            }
        }
    }
    class Admin : Personal
    {
        public Admin(string name) : base(name)
        {

        }
        public static implicit operator Admin(string name)
        {
            return new Admin(name);
        }
        public static explicit operator string(Admin raven)
        {
            return raven.name;
        }
    }
    class Pancacke : Dish
    {
        public Pancacke(string name) : base(name)
        {
            base.Eat(this);
        }
        public void Prepare(out string oven)
        {
            oven = "dish is cooked";
        }
    }
    enum Type
    {
        Small,
        Medium,
        Big,
        Large
    }
    class Pizza : Dish
    {
        readonly Type type;
        readonly int age;
        public Pizza(string name, Type type)
        {
            this.name = name;
            this.type = type;
        }
        //розібратись
        public bool CheckBigDog()
        {
            Type BigPizza = Type.Big | Type.Large;
            return ((type & BigPizza) == BigPizza) && ((BigPizza ^ Type.Small & Type.Medium) == type);
        }
    }
    class Waiter : Personal,IStuff
    {
        public Waiter(string name) : base(name)
        {
        }

        public void Work(IStuff stuff)
        {
            if(stuff is Waiter)
            {
                Console.WriteLine(((Waiter)stuff).name + ": I'm a waiter!");
            }
        }
        public void BringOrder(ref string work)
        {
            work = "bring order";
        }
    }
    class Pub
    {
        private Pub instance = null;
        public string name { get; }
        private Pub(string name)
        {
            this.name = name;
        }
        private class Office
        {
            private string type;
            public Office(string type)
            {
                this.type = type;
            }
        }
        public Pub GetInstance(string name)
        {
            if(instance == null)
            {
                instance = new Pub(name);
            }
            return instance;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string health;
            Parrot parrot = new Parrot("dfgwegwr", "egthwrhet");
            parrot.ToIll(out health);
            Console.WriteLine(health);
            Doctor doctor = new Doctor();
            doctor.Heal(ref health);
            Console.WriteLine(health);
            int number = 55;//boxing/unbo5xing
            object o = number;
            o = 60;
            number = (int)o;
            Console.WriteLine(number);
            Console.ReadLine();
        }
    }
}
