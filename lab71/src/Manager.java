import java.util.*;


public class Manager {
    private int dist[];
    private Set<Integer> settled;
    private PriorityQueue<Node> pq;
    private int V; // Number of vertices
    private static int[][] adjacencyList = {
            {1,9},
            {2,6},
            {3,5},
            {4,3},
            {1,2},
            {3,4}};
    List<List<Node> > adj;



    public Manager(int V)
    {
        this.V = V;
        dist = new int[V];
        settled = new HashSet<Integer>();
        pq = new PriorityQueue<Node>(V, new Node());
    }
    private void dijkstra(List<List<Node> > adj, int src)
    {
        this.adj = adj;
        for (int i = 0; i < V; i++)
            dist[i] = Integer.MAX_VALUE;
        pq.add(new Node(src, 0));
        dist[src] = 0;
        while (settled.size() != V) {
            if(pq.isEmpty())
                return ;
            int u = pq.remove().node;
            settled.add(u);
            e_Neighbours(u);
        }
    }
    private void e_Neighbours(int u)
    {
        int edgeDistance = -1;
        int newDistance = -1;
        for (int i = 0; i < adj.get(u).size(); i++) {
            Node v = adj.get(u).get(i);
            if (!settled.contains(v.node)) {
                edgeDistance = v.cost;
                newDistance = dist[u] + edgeDistance;
                if (newDistance < dist[v.node])
                    dist[v.node] = newDistance;
                pq.add(new Node(v.node, dist[v.node]));
            }
        }
    }


    public static void method( )
    {
        int V = 5;
        int source = 0;
        List<List<Node> > adj = new ArrayList<List<Node> >();
        for (int i = 0; i < V; i++) {
            List<Node> item = new ArrayList<Node>();
            adj.add(item);
        }
        adj.get(0).add(new Node(adjacencyList[0][0], adjacencyList[0][1]));
        adj.get(0).add(new Node(adjacencyList[1][0], adjacencyList[1][1]));
        adj.get(0).add(new Node(adjacencyList[2][0], adjacencyList[2][1]));
        adj.get(0).add(new Node(adjacencyList[3][0], adjacencyList[3][1]));
        adj.get(2).add(new Node(adjacencyList[4][0], adjacencyList[4][1]));
        adj.get(2).add(new Node(adjacencyList[5][0], adjacencyList[5][1]));
        Manager manager = new Manager(V);
        manager.dijkstra(adj, source);
        System.out.println("The shorted path from node :");
        for (int i = 0; i < manager.dist.length; i++)
            System.out.println(source + " to " + i + " is "
                    + manager.dist[i]);
    }
}

class Node implements Comparator<Node> {

    public int node;
    public int cost;



    public Node()
    {
    }
    public Node(int node, int cost)
    {
        this.node = node;
        this.cost = cost;
    }
    @Override
    public int compare(Node node1, Node node2)
    {
        if (node1.cost < node2.cost)
            return -1;
        if (node1.cost > node2.cost)
            return 1;
        return 0;
    }
}