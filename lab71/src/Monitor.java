public class Monitor {
    private String name;
    private StopWatch stopWatch;
    private int priority;

    public Monitor(String name, StopWatch stopWatch,int priority) {
        this.name = name;
        this.priority = priority;
        this.stopWatch = stopWatch;
    }
}
