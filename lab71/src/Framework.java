import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;
import static java.util.concurrent.TimeUnit.*;

public class Framework extends JFrame{
    private JPanel mainPanel;
    private JTextField nameTextField;
    private JTextField priorityTextField;
    private JButton deleteThreadButton;
    private JButton startButton;
    private JButton addButton;
    private JTable table;
    private JPanel textPanel;
    private JLabel nameLabel;
    private JLabel priorityLabel;
    private JPanel buttonPanel;
    private JScrollPane tableScrollPanel;
    private JLabel resultTimeLabel;
    private JLabel timeLabel;
    private JPanel timePanel;
    //model
    private DefaultTableModel model;
    private String[] columns = { "Name","Priority","State"};
    private String[] row;
    //list threads
    ArrayList<MyThread> threads = new ArrayList<>();
    ArrayList<String> names = new ArrayList<>();
    //timer
    StopWatch stopWatch = new StopWatch();
    private ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();


    public Framework()
    {
        //model for table
        model = new DefaultTableModel();
        model.setColumnIdentifiers(columns);
        table.setModel(model);
        //mainPanel
        setContentPane(mainPanel);
        //frame
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        setBounds(dimension.width/2 - 350 ,dimension.height/2 - 200, 700, 400);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);



        //add thread
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(nameTextField.getText().equals("") || priorityTextField.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Fill all text fields please");
                }
                else {
                    if (threads.isEmpty()) {
                        service.schedule(new MyThread(nameTextField.getText(), Integer.parseInt(priorityTextField.getText())),3, TimeUnit.SECONDS);
                        threads.add(new MyThread(nameTextField.getText(), Integer.parseInt(priorityTextField.getText())));
                        names.add(nameTextField.getText());
                        row = new String[3];
                        row[0] = nameTextField.getText();
                        row[1] = priorityTextField.getText();
                        row[2] = "NEW";
                        //fill table
                        model.addRow(row);
                    } else {
                        boolean state = true;
                        for (MyThread thread : threads) {
                            if (names.contains(nameTextField.getText())) {
                                JOptionPane.showMessageDialog(null, "Thread with this name already created");
                                state = false;
                                break;
                            }
                        }
                        if(state) {
                            threads.add(new MyThread(nameTextField.getText(), Integer.parseInt(priorityTextField.getText())));
                            row = new String[4];
                            row[0] = nameTextField.getText();
                            row[1] = priorityTextField.getText();
                            row[2] = "NEW";
                            row[3] = "0";
                            //fill table
                            model.addRow(row);
                        }
                    }
                    nameTextField.setText("");
                    priorityTextField.setText("");
                }
            }
        });


        //delete thread

        deleteThreadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int i = table.getSelectedRow();
                if(i>=0) {
                    model.removeRow(i);
                    JOptionPane.showMessageDialog(null,"delete successfully");

                }else
                    JOptionPane.showMessageDialog(null,"Please select a row");
            }
        });

        //start
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (threads.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Firstly add thread");
                } else {
                    stopWatch.start();
                    Semaphore semaphore = new Semaphore(1);
                    for (MyThread thread : threads) {
                        thread.semaphore = semaphore;
                    }
                    int i = 0;
                    int j;
                    //                        service.shutdown();
                    for (MyThread thread : threads) {
                        j = i+1;
                        model.setValueAt("Running",i,2);
                        model.setValueAt("Waiting",j,2);
                        thread.start();
                        model.setValueAt("End",i,2);
                        i++;
                    }
                    stopWatch.stop();
                    resultTimeLabel.setText(String.valueOf(stopWatch.getTime()));
                }
            }
        });
    }

    public ArrayList<String> getNames() {
        return names;
    }

    public static void main(String[] args) {
        Framework framework = new Framework();
    }

}
