import test.Manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class MyThread extends Thread {
    Semaphore semaphore;
    private String name;
    private int priority;
    private int i = 0;
    public MyThread(String name, int priority)
    {
        this.name = name;
        this.priority = priority;
    }
    public  void run() {
        try {
            semaphore.acquire();
            System.out.println( currentThread().getName() + "   priority:" + currentThread().getPriority());
            Manager.method();
            i++;
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public String getThreadName() {
        return name;
    }

}
